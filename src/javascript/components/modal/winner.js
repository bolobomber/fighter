import { showModal } from "./modal";

export function showWinnerModal(fighter) {
  const winnerInfo = {
    title: '!!!ПЕРЕМОГА!!!!',
    bodyElement: ` переможець - ${fighter.name}`
  }
  
  showModal(winnerInfo);
}